"""This profile instantiates the vendor AP-to-Edge setup as connected via the PhantomNet attenuator matrix.  It includes a NAT proxy node that connects to the internal (experimental net) LAN that the devices communicate on.

Instructions:
Swap in.
"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.igext as ig
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.pnext as pn

MATRIX_APS = ["cap1", "cap2"]
DENSE_APS = ["cap-wasatch", "cap-ustar", "cap-ebc"]
AP_DEVICES = MATRIX_APS + DENSE_APS
EDGE_DEVICES = ["ceg1",]
UE_NODES = ["nuc22",]
PROXY_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-focal-image"
PROXY_SCRIPT = "/local/repository/bin/proxy_start.sh"

PROXYGWADDR = "192.168.2.1"
SHVLANNAME = "CEGCAP"
SHVLANMASK = "255.255.255.0"

# Parameters
pc = portal.Context()

# Set of AP devices to allocate
portal.context.defineStructParameter(
    "ap_devices", "Access Point Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Access Point Devices.",
    members=[
        portal.Parameter(
            "id",
            "AP Device",
            portal.ParameterType.STRING,
            AP_DEVICES[0], AP_DEVICES,
            longDescription="AP radio devices to allocate/connect via RF matrix."
        ),
    ])

# Set of Edge devices to allocate
portal.context.defineStructParameter(
    "edge_devices", "Edge Devices", [{"id": "ceg1"}],
    multiValue=True,
    min=1,
    multiValueTitle="Edge Devices.",
    members=[
        portal.Parameter(
            "id",
            "Edge Device",
            portal.ParameterType.STRING,
            EDGE_DEVICES[0], EDGE_DEVICES,
            longDescription="Edge devices to allocate/connect."
        ),
    ])

# Set of UE devices to allocate
portal.context.defineStructParameter(
    "ue_nodes", "UE Nodes", [],
    multiValue=True,
    min=0,
    multiValueTitle="UE Nodes.",
    members=[
        portal.Parameter(
            "id",
            "UE Node",
            portal.ParameterType.STRING,
            UE_NODES[0], UE_NODES,
            longDescription="NUC + COTS UE modem devices to allocate/connect via RF matrix."
        ),
    ])

pc.defineParameter("proxy_hwtype", "Proxy Node Type",
                   portal.ParameterType.STRING, "d710",
                   longDescription="Hardware type for compute node that will act as the proxy. Leave blank for 'any'.")

pc.defineParameter("apn_name", "APN/DNN for COTS UE to use.",
                   portal.ParameterType.STRING, "celonadns",
                   longDescription="The Quectel connection manager will use this APN/DNN to obtain a PDU session.",
                   advanced=True)
pc.defineParameter("gwaddr", "Gateway address for proxy on shared vlan",
                   portal.ParameterType.STRING, PROXYGWADDR,
                   advanced=True)
pc.defineParameter("sharedVlanName","Shared VLAN Name",
                   portal.ParameterType.STRING,SHVLANNAME,
                   longDescription="This is the name of the shared VLAN that the separate AP experiments will attach to.",
                   advanced=True)
pc.defineParameter("sharedVlanNetmask", SHVLANMASK,
                   portal.ParameterType.STRING,"255.255.255.0",
                   advanced=True)
pc.defineParameter("createSharedVlan", "Create Shared VLAN",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Create the shared VLAN. (Disable this if the VLAN already exists.)",
                   advanced=True)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

# Create the "fronthaul LAN"
lan1 = request.LAN("lan1")
lan1.vlan_tagging = False
lan1.setNoBandwidthShaping()
if params.createSharedVlan:
    lan1.createSharedVlan(params.sharedVlanName)
else:
    lan1.connectSharedVlan(params.sharedVlanName)

# Request the NAT proxy node.
proxy = request.RawPC("proxy")
proxy.hardware_type = params.proxy_hwtype
proxy.disk_image = PROXY_IMG
proxy.addService(pg.Execute(shell="sh", command=PROXY_SCRIPT))
pxlif = proxy.addInterface("pxlif", pg.IPv4Address(params.gwaddr, params.sharedVlanNetmask))
lan1.addInterface(pxlif)

# Request the Edge devices.
for device in params.edge_devices:
    edge = request.RawPC(device.id)
    edge.component_id = device.id
    lan1.addNode(edge)

# Request the AP devices
matrix_ap_nodes = []
for device in params.ap_devices:
    ap = request.RawPC(device.id)
    ap.component_id = device.id
    if device.id in MATRIX_APS:
        matrix_ap_nodes.append(ap)
        ap.Desire("rf-controlled", 1)
    lan1.addNode(ap)

# Request the nodes with the 5G module
ue_nodes = []
for idx,node in enumerate(params.ue_nodes):
    ue = request.RawPC("ue{}".format(idx+1))
    ue_nodes.append(ue)
    ue.component_id = node.id
    ue.disk_image = COTS_UE_IMG
    ue.Desire("rf-controlled", 1)
    ue.addService(pg.Execute(shell="sh", command="sudo cp /local/repository/etc/default.script /etc/udhcpc/"))
    ue.addService(pg.Execute(shell="bash", command="/local/repository/bin/add-quectelcm-service.sh {}".format(params.apn_name)))

# Set attenuation to max on all APs if more then one.
if len(matrix_ap_nodes) > 1:
    ue0 = ue_nodes[0]
    for ap in matrix_ap_nodes:
        ue0.addService(pg.Execute(shell="bash", command="/local/repository/bin/update-attens {} 95".format(ap.name)))
        pass
    pass

# Create the RF links between the COTS 5G modules and AP devices
idx = 1
for ap in matrix_ap_nodes:
    for ue in ue_nodes:
        rflink = request.RFLink("rflink{}".format(idx))
        rflink.addInterface(ap.addInterface("{}rf{}".format(ap.name,idx)))
        rflink.addInterface(ue.addInterface("{}rf{}".format(ue.name,idx)))
        idx += 1

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
